<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

	<xsl:template match="/">
		<html>
			<body>
				<!-- The heading -->
				<div style="font-family:Comic Sans MS;
				            font-size:36pt;
				            font-weight:bold;
 				            color:red;
				            text-align:center">
                                       The Cars List
				</div>
				<br/>
				<!-- Define the table itself -->
				<table width="100%" border="2">
				 <xsl:apply-templates select="//car"
				   order-by="+@make; +@model; +subModel/@value; engine/@capacity"/>
				</table>
			</body>
		</html>
	</xsl:template>

	<!-- Each row in the table is a separate car -->
	<xsl:template match="car">
		<tr valign="top">

			<!-- Display the "make" in the first column -->
			<td style="font-family:Comic Sans MS;
			           font-size:12pt;
			           color:darkblue">
				<xsl:apply-templates select="@make"/>
			</td>

			<!-- Display the "model" in the second column -->
			<td style="font-family:Comic Sans MS;
			           font-size:12pt;
			           color:darkblue">
				<xsl:apply-templates select="@model"/>
				<xsl:apply-templates select="subModel"/>
			</td>

			<!-- Display the "engine/gearbox/turbo" information -->
			<td style="font-family:Comic Sans MS;
			           font-size:12pt;
			           color:darkgreen">
				<xsl:apply-templates select="engine"/>
				<xsl:apply-templates select="gearbox"/>
			</td>

			<td style="font-family:Comic Sans MS;
			           font-size:12pt;
			           color:darkgreen">
				<xsl:apply-templates select="@colour"/>
			</td>

			<td style="font-family:Comic Sans MS;
			           font-size:12pt;
			           color:orange">
				<xsl:apply-templates select="option"/>
				<xsl:entity-ref name='nbsp'/>
				 <!-- This indents the cell if there are no contents -->
			</td>
		</tr>
	</xsl:template>

	<!-- How to display the "make" attribute -->
	<xsl:template match="@make">
		<xsl:value-of/>
	</xsl:template>

	<xsl:template match="@model">
		<xsl:value-of/>
	</xsl:template>

	<!-- How to display the "subModel" elelent -->
	<xsl:template match="subModel">
		<xsl:value-of select="@value"/>
	</xsl:template>

	<!-- How to display the "value" attribute of the "subModel" element-->
	<xsl:template match="@value">
		<xsl:value-of/>
	</xsl:template>

	<!-- How to display the "engine" element -->
	<xsl:template match="engine">
		<xsl:apply-templates select="@capacity"/>
		<xsl:apply-templates select="@type"/>
		<xsl:apply-templates select="turbo"/>
	</xsl:template>

	<xsl:template match="@capacity">
		<xsl:value-of/>cc
	</xsl:template>

	<xsl:template match="engine/@type">
		<xsl:choose>
		 <xsl:when match="engine/@type[. $ne$ 'petrol']"><xsl:value-of/></xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="turbo">
		Turbo <xsl:apply-templates select="@model"/>
	</xsl:template>

	<xsl:template match="@model">
		<xsl:value-of/>
	</xsl:template>

	<xsl:template match="gearbox">
		<xsl:apply-templates select="@type"/>
	</xsl:template>

	<xsl:template match="gearbox/@type">
		<xsl:choose>
		 <xsl:when match="@type[. $eq$ 'auto']">Auto</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@colour">
		<xsl:value-of/>
	</xsl:template>

	<xsl:template match="option">
		<xsl:if match="option[index() != 0]">
			,<br/>
		</xsl:if>
		<xsl:apply-templates select="@type"/>
	</xsl:template>

	<!-- How to display the "option type" attribute -->
	<xsl:template match="option/@type">
		<xsl:value-of/>
	</xsl:template>

</xsl:stylesheet>

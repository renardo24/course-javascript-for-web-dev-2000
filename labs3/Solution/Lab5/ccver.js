/******************************************************************************\

  This file contains two routines for validating the modulo10 checksum of
  creditcard numbers; evenModulo validates numbers with even numbers of digits
  and oddModulo validates numbers with odd numbers of digits.

  Note that it would be very simple to collapse these two routines into a single
  one; they have been kept separate for the sake of clarity.

  Author: Paul Fletcher
  Copyright (C) QA Training Ltd 1997

\******************************************************************************/


// Check the mod10 checksum of a number with an even number of digits
// return true if it passes muster, else false.

function evenModulo(number)
{
	var	sum = 0;
	var	i;
	/**/
	
		/* handle the hot digits first */
	for (i = 0; i < number.length; i += 2)
	{
		temp = 2 * number.charAt(i);
		sum += temp % 10;
			/* this is another way of doing sum += temp div 10; JavaScript lacks
			   integer division.  However, the maximum possible value
			   of the original digit is 9, and thus the maximum possible value
			   of temp (2 * digit) is 18.  So, temp div 10 can only be 0 or 1.
			   If temp is greater than or equal to 10 then temp div 10 is 1, and we
			   add that to sum
			*/
		if (temp >= 10)
			sum++;
	}

		/* now add in the other digits; beware: mixing strings and numbers
		   with the + operator will do string addition rather than number
		   additon; so we use parseInt to force both operands to numbers
		*/
	for (i = 1; i < number.length; i+=2)
		sum += parseInt(number.charAt(i));
		
	return((sum % 10) == 0);
}



// Check the mod10 checksum of a number with an odd number of digits
// return true if it passes muster, else false.

function oddModulo(number)
{
	var	sum = 0;
	var	i;
	/**/
	
		/* handle the hot digits first */
	for (i = 1; i < number.length; i += 2)
	{
		temp = 2 * number.charAt(i);
		sum += temp % 10;
			/* this is another way of doing sum += temp div 10; JavaScript lacks
			   integer division.  However, the maximum possible value
			   of the original digit is 9, and thus the maximum possible value
			   of temp (2 * digit) is 18.  So, temp div 10 can only be 0 or 1.
			   If temp is greater than or equal to 10 then temp div 10 is 1, and we
			   add that to sum
			*/
		if (temp >= 10)
			sum++;
	}

		/* now add in the other digits; beware: mixing strings and numbers
		   with the + operator will do string addition rather than number
		   additon; so we use parseInt to force both operands to numbers
		*/
	for (i = 0; i < number.length; i+=2)
		sum += parseInt(number.charAt(i));
		
	return((sum % 10) == 0);
}




var WSHShell = WScript.CreateObject("WScript.Shell");
// Read desktop path using WshSpecialFolders object
var DesktopPath = WSHShell.SpecialFolders("Desktop");

// Create a shortcut object on the desktop
var shortCut = WSHShell.CreateShortcut(DesktopPath + "\\Shortcut to notepad.lnk");

// Set shortcut object properties and save it
shortCut.TargetPath = WSHShell.ExpandEnvironmentStrings("%windir%\\notepad.exe");
shortCut.WorkingDirectory = WSHShell.ExpandEnvironmentStrings("%windir%");
shortCut.WindowStyle = 4;
shortCut.IconLocation = WSHShell.ExpandEnvironmentStrings("%windir%\\notepad.exe, 0");
shortCut.Save();

WScript.Echo("A shortcut to Notepad now exists on your Desktop.");


var	_console = window.open("", "console", "width=600,height=200,scrollbars=1,resizable=1");

_console.document.open("text/plain");
_console.document.writeln("Debug session started " + new Date());


function debug()
{
	for (var i = 0; i < arguments.length; i++)
		_console.document.write(arguments[i]);

	_console.document.writeln("");
}

function debugexit()
{
	_console.document.close();
	_console.close();
}

function debugclear()
{
	_console.document.close();
	_console.document.open("text/plain");
}

function debugfocus()
{
	_console.focus();
}

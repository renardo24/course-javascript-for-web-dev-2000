import java.awt.*;

public class MyFrame extends Frame
{
	public MyFrame()
	{
		super();
	}

	public MyFrame(String title)
	{
		super(title);
	}

	public boolean handleEvent(Event e)
	{
		if (e.id == Event.WINDOW_DESTROY)
			System.exit(0);
		return super.handleEvent(e);
	}

	public static void main(String[] args)
	{
		Frame f = new MyFrame("Test Frame");
		f.resize(300, 200);
		f.show();
	}
}
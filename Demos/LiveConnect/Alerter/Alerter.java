import java.applet.Applet;
import java.awt.*;
import netscape.javascript.*;

public class Alerter extends Applet
{

	public void start()
	{
		JSObject window = JSObject.getWindow(this);
		window.eval("alert('The Alerter applet is now running');");
	}

	public void paint(Graphics g)
	{
		g.drawString("This applet can interact with Netscape Navigator", 0, 50);
	}
}


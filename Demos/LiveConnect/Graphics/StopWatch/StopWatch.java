// StopWatch

import java.util.*;
import java.awt.*;
import java.applet.*;

public class StopWatch extends Applet implements Runnable
{
	Thread timer = null;
	int lastxs=0, lastys=0, lastxm=0, lastym=0, lastxh=0, lastyh=0;
	long startTime = 0, elapsedTime = 0;
	long lastTime = 0;
	
	public void init() {
	  int x,y;
	  resize(300,300);              // Set clock window size
	}

	  // Plotpoints allows calculation to only cover 45 degrees of the circle,
	  // and then mirror

	public void plotpoints(int x0, int y0, int x, int y, Graphics g) {

	  g.drawLine(x0+x,y0+y,x0+x,y0+y);
	  g.drawLine(x0+y,y0+x,x0+y,y0+x);
	  g.drawLine(x0+y,y0-x,x0+y,y0-x);
	  g.drawLine(x0+x,y0-y,x0+x,y0-y);
	  g.drawLine(x0-x,y0-y,x0-x,y0-y);
	  g.drawLine(x0-y,y0-x,x0-y,y0-x);
	  g.drawLine(x0-y,y0+x,x0-y,y0+x);
	  g.drawLine(x0-x,y0+y,x0-x,y0+y);
	}

	  // Circle is just Bresenham's algorithm for a scan converted circle

	public void circle(int x0, int y0, int r, Graphics g) {
	  int x,y;
	  float d;

	  x=0;
	  y=r;
	  d=5/4-r;
	  plotpoints(x0,y0,x,y,g);

	  while (y>x){
		if (d<0) {
		  d=d+2*x+3;
		  x++;
		}
		else {
		  d=d+2*(x-y)+5;
		  x++;
		  y--;
		}
		plotpoints(x0,y0,x,y,g);
	  }
	}


	  // Paint is the main part of the program

	public void paint(Graphics g) {
	  int xh, yh, xm, ym, xs, ys, s, m, h, xcenter, ycenter;

	  if (timer != null)
	  {
	    long currentTime = System.currentTimeMillis()/1000;
	    elapsedTime = currentTime - startTime;
	  }

	  s = (short)elapsedTime;
	  xcenter=80;
	  ycenter=55;
  
	  // a= s* pi/2 - pi/2 (to switch 0,0 from 3:00 to 12:00)
	  // x = r(cos a) + xcenter, y = r(sin a) + ycenter
  
	  xs = (int)(Math.cos(s * 3.14f/30 - 3.14f/2) * 45 + xcenter);
	  ys = (int)(Math.sin(s * 3.14f/30 - 3.14f/2) * 45 + ycenter);
	  //xm = (int)(Math.cos(m * 3.14f/30 - 3.14f/2) * 40 + xcenter);
	  //ym = (int)(Math.sin(m * 3.14f/30 - 3.14f/2) * 40 + ycenter);
  
	  // Draw the circle and numbers
  
	  g.setFont(new Font("TimesRoman", Font.PLAIN, 14));
	  g.setColor(Color.blue);
	  circle(xcenter,ycenter,50,g);
	  g.setColor(Color.blue);
	  g.drawString("0",xcenter-3,ycenter-37);
	  g.drawString("15",xcenter+35,ycenter+3);
	  g.drawString("30",xcenter-5,ycenter+45);
	  g.drawString("45",xcenter-45,ycenter+3);

	  // Erase if necessary, and redraw
  
	  g.setColor(Color.lightGray);
	  if (xs != lastxs || ys != lastys) {
		g.drawLine(xcenter, ycenter, lastxs, lastys);
		g.drawString("" + lastTime, xcenter-5, ycenter+75);
	  }
	  //if (xm != lastxm || ym != lastym) {
		//g.drawLine(xcenter, ycenter-1, lastxm, lastym);
		//g.drawLine(xcenter-1, ycenter, lastxm, lastym); }
	  g.setColor(Color.darkGray);
	  g.drawString("" + elapsedTime, xcenter-5, ycenter+75);
	  g.setColor(Color.red);
	  g.drawLine(xcenter, ycenter, xs, ys);
	  g.setColor(Color.blue);
	  //g.drawLine(xcenter, ycenter-1, xm, ym);
	  //g.drawLine(xcenter-1, ycenter, xm, ym);
	  lastxs=xs; lastys=ys;
	  //lastxm=xm; lastym=ym;
	  lastTime = elapsedTime;
	}

	public void startTiming() {
	  if(timer == null)
	  {
	      startTime = System.currentTimeMillis()/1000;

		  timer = new Thread(this);
		  timer.start();
	  }
	}

	public void stopTiming() {
	  timer.stop();
	  timer = null;
	}

	public void run() {
	  while (timer != null) {
		try
		{
		  Thread.sleep(100);
		}
		catch (InterruptedException e)
		{}
		repaint();
	  }
	  timer = null;
	}

	public void update(Graphics g) {
	  paint(g);
	}
}


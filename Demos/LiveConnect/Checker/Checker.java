import java.applet.Applet;
import java.awt.*;
import netscape.javascript.*;

public class Checker extends Applet
{
	JSObject window;
	boolean thisChecked = false, lastChecked = true;

	public void start()
	{
		window = JSObject.getWindow(this);
	}

	public void refresh()
	{
		repaint();
	}

	public void paint(Graphics g)
	{
		thisChecked = ((Boolean)window.eval
				("document.form1.checkbox1.checked")).booleanValue();

		if (thisChecked != lastChecked)
		{
			if (thisChecked)
				g.drawString("The checkbox is checked", 0, 50);
			else
				g.drawString("The checkbox is unchecked", 0, 50);
			lastChecked = thisChecked;
		}
	}
}


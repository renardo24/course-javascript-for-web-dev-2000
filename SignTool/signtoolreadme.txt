                   Netscape Signing Tool (signtool)
                         1.0 Release Notes
               ========================================

See the file license.txt for licensing information.

Documentation is provided online at:
   http://developer.netscape.com/library/documentation/signedobj/signtool/

Problems or questions not covered by the online documentation can be
discussed in the DevEdge Security Newsgroup, or addressed to
signobj-support@netscape.com.

Zigbert 0.6 Support
===================
This program was previously named Zigbert.  The last version of zigbert
was Zigbert 0.6.  Because all the functionality of Zigbert is maintained in
signtool 1.0, Zigbert is no longer supported.  If you have problems
using Zigbert, please upgrade to signtool 1.0.

New Features in 1.0
===================

Creation of JAR files
----------------------
The -Z option causes signtool to output a JAR file formed by storing the
signed archive in ZIP format.  This eliminates the need to use a separate ZIP
utility.  The -c option specifies the compression level of the resulting
JAR file.

Generation of Object-Signing Certificates and Keys
--------------------------------------------------
The -G option will create a new, self-signed object-signing certificate
which can be used for testing purposes.  The generated certificate and 
associated public and private keys will be installed in the cert7.db and
key3.db files in the directory specified with the -d option. On Unix systems,
if no directory is specified, the user's Netscape directory (~/.netscape)
will be used. In addition, the certificate is output in X509 format to the
files x509.raw and x509.cacert in the current directory.  x509.cacert can
be published on a web page and imported into browsers that visit that page.

Extraction and Signing of JavaScript from HTML
----------------------------------------------
The -J option activates the same functionality provided by the signpages
Perl script.  It will parse a directory of html files, creating archives
of the JavaScript called from the HTML. These archives are then signed and
made into JAR files.

Enhanced Smart Card Support
---------------------------
Certificates that reside on smart cards are displayed when using the -L and
-l options.

Known Limitations of 1.0
========================

Certificate Generation
----------------------
The -G option creates an RSA certificate with a 512-bit key length.  There
is no way to change the type or key length of the generated certificate.
The -G option uses the internal Netscape security module to generate
certificates, and will not generate certificates on a smart card or any
other PKCS #11 module.  It will use the FIPS-validated module if that
module has been installed into the security module database.  See the 
SignTool 1.0 documentation for information on configuring SignTool for
FIPS-140-1 operation.
